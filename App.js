/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect, useMemo} from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  LogBox,
} from 'react-native';
import FastImage from "react-native-fast-image";
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { navigationRef } from './RootNavigation';
import { SvgCssUri } from 'react-native-svg';
import AsyncStorage from '@react-native-community/async-storage';
import {AuthContext} from "./Components/Auth/authContext";
import {NavigationContainer} from "@react-navigation/native";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
console.disableYellowBox = true;
console.error = (error) => error.apply;
LogBox.ignoreAllLogs();
import loadingPage from "./Components/Auth/loadingPage";
import Profile from "./Components/Profile";
import HomeScreen from "./Components/HomeScreen";
import SignIn from "./Components/Auth/SignIn";
import Tracking from "./Components/Tracking";
import Notification from "./Components/Notification";

const HomeStack = createNativeStackNavigator();
const AuthStack = createNativeStackNavigator();
const loadingStack = createNativeStackNavigator();

function loadingScreen() {
  return(
      <loadingStack.Navigator  screenOptions={{ headerShown: false }}>
        <loadingStack.Screen name='loadingPage' component={loadingPage}/>
      </loadingStack.Navigator>
  )
}

function authScreen() {
  return(
      <AuthStack.Navigator screenOptions={{ headerShown: false }} initialName='SignIn'>
        <AuthStack.Screen name='SignIn' component={SignIn}/>
      </AuthStack.Navigator>
  )
}


function homeScreen({ navigation, route }) {
  if (route.state && route.state.index > 0) {
    navigation.setOptions({ tabBarVisible: false })
  } else {
    navigation.setOptions({ tabBarVisible: true })
  }
  return(
      <HomeStack.Navigator  screenOptions={{ headerShown: false }}>
        <HomeStack.Screen name='HomeScreen' component={HomeScreen}/>
        <HomeStack.Screen name='Profile' component={Profile}/>
      </HomeStack.Navigator>
  )
}

const Tab = createBottomTabNavigator();
let image_icon;
let fill_icon = '#000';
function TabBottomScreen(props){
  return(
      <Tab.Navigator screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
              let image_icon;

              if (route.name === 'HomeScreen') {
                  fill_icon = focused ? '#0EA3F7':'#000';
                  image_icon = 'home.svg';
              } else if (route.name === 'Tracking') {
                  fill_icon = focused ? '#0EA3F7':'#000';
                  image_icon = 'home.svg';
              } else if (route.name === 'Notification') {
                  fill_icon = focused ? '#0EA3F7':'#000';
                  image_icon = 'notification.svg';
              } else if (route.name === 'Profile') {
                  fill_icon = focused ? '#0EA3F7':'#000';
                  image_icon = 'user.svg';
              }
              // You can return any component that you like here!
              return <SvgCssUri
                  width={24}
                  height={22}
                  fill={fill_icon}
                  stroke={fill_icon}
                  uri= {"https://build.viin.vn/img_kizuna/" + image_icon}
              />
          }
      })}
                     tabBarOptions={{
                         activeTintColor: '#008bde',
                         labelStyle: {
                             fontWeight: 'bold',
                             paddingBottom: 0
                         },
                         style: {
                             shadowColor: '#000',
                             shadowOffset: { width: 1, height: 2 },
                             shadowOpacity: 0.1,
                             shadowRadius: 3,
                             elevation: 5,
                         }
                     }}>
          <Tab.Screen name="HomeScreen" component={HomeScreen} options={{
              tabBarLabel: 'Home',
              headerShown: false
          }}
          />
          <Tab.Screen name="Tracking" component={Tracking} options={{
              tabBarLabel: 'Tracking',
              headerShown: false
          }}
          />
          <Tab.Screen name="Notification" component={Notification} options={{
              tabBarLabel: 'Thông báo',
              headerShown: false
          }}
          />
          <Tab.Screen name="Profile" component={Profile} options={{
              tabBarLabel: 'Thông tin',
              headerShown: false
          }}
          />
      </Tab.Navigator>
  )
}

function App(){
  const [isLoading, setIsLoading] = React.useState(true);
  const [Token, setToken] = React.useState(null);
  const [domain, setDomain] = React.useState(null);
  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setIsLoading(false);
        setToken('1');
      },
      Register: () => {
        setIsLoading(false);
        setToken(null);
      },
      Logout: () => {
        setIsLoading(false);
        setToken(null);
      }
    }
  }, []);

  const bootstrapAsync = async () => {
    let userToken;
    let domainLogin;
    try {
      domainLogin = await AsyncStorage.getItem('domain');
      userToken = await AsyncStorage.getItem('userToken');
      setToken(userToken);
      setDomain(domainLogin);
    } catch (e) {
      // Restoring token failed
    }
  };
  React.useEffect( () => { // thay thế componentDidmount và componentDidUpdate
    bootstrapAsync();
    setTimeout(() => {
      setIsLoading(false);
    }, 2000);
  }, []);

  return (
      <AuthContext.Provider value={authContext}>
        <NavigationContainer>
          {  isLoading ? loadingScreen() :
              Token ? TabBottomScreen() : authScreen()
          }
        </NavigationContainer>
      </AuthContext.Provider>


  );
}


export default App;
