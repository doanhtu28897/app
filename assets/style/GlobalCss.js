import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
    container: {
        height: '100%'
    },
    bgColorLoadingPage: {
        backgroundColor: '#4A4CCD'
    },
    bgColorSignIn: {
        backgroundColor: '#E5E5E5'
    },
    bgColorWhite: {
        backgroundColor: '#fff'
    },
    bgColorProfile: {
        backgroundColor: '#F0EBFF'
    },
    view_title_header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 15,
        paddingTop: Platform.OS === 'ios'?0:15
    },
    iconBack: {
        width: '20%',
        paddingLeft: 10
    },
    titleWidth: {
        alignItems: 'center'
    },
    title_header_text: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 20,
        textTransform: 'uppercase'
    },
    iconRight: {
        fontSize: 20,
        color: '#DEDEDE'
    },
    flexRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginLeft: 20,
        width: '80%'
    },
    iconHome: {
        height: 30,
        width: 30
    },
    viewIconHome: {
        backgroundColor: '#5F27CD',
        padding: 10,
        borderRadius: 30
    },
    touchViewHome: {
        borderRadius: 10,
        marginBottom: 10,
        flexDirection: 'row',
        padding: 15,
        alignItems: 'center'
    },
    viewBgProfile: {
        height: '40%',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        justifyContent: 'space-around',
        padding: 20
    },
    touchViewProfile: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    viewIconProfile: {
        backgroundColor: '#FDD9E5',
        padding: 10,
        borderRadius: 10
    },
    viewNotiProfile: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '80%',
        marginLeft: 20
    },
    avatarProfile: {
        height: 100,
        width: 100,
        borderRadius: 50
    },
    viewAvatarProfile: {
        alignItems: 'center',
        alignSelf: 'center',
        padding: 5,
        borderRadius: 50,
        marginBottom: 20
    },
    nameProfile: {
        alignSelf: 'center',
        fontSize: 20,
        color: '#2C1F44'
    },
    viewMemberProfile: {
        alignItems: 'center',
        alignSelf: 'center',
        padding: 10,
        borderRadius: 20,
        width: '30%',
        marginTop: 20
    },
    textMemberProfile: {
        color: '#FC427B',
        fontWeight: 'bold',
        fontSize: 15
    }
})
