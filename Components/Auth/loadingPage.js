import React from 'react';
import {View, Text} from 'react-native';
import FastImage from "react-native-fast-image";
import {useNavigation} from '@react-navigation/native'
import GlobalCss from "../../assets/style/GlobalCss";
export default function loadingPage(props) {
    const navigation = useNavigation();
    return(
        <View style={[GlobalCss.container, GlobalCss.bgColorLoadingPage, {justifyContent: 'center', alignItems: 'center'}]}>
            <FastImage source={{uri: 'https://tacorp.vn/wp-content/uploads/2020/06/LOGO-THIEN-AN-small.png'}}
                       style={{height: 200, width: '70%'}} resizeMode='contain'/>
                       <Text style={{fontSize: 30, fontWeight: 'bold', color: '#fff'}}>Thiên Ân Corp</Text>
        </View>
    )
}
