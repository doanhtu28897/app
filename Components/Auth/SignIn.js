import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, SafeAreaView, Text} from 'react-native';
import FastImage from "react-native-fast-image";
import {Icon, Item, Input, Button, Form} from 'native-base'
import GlobalCss from '../../assets/style/GlobalCss'
import {useNavigation} from '@react-navigation/native'
import {AuthContext} from "./authContext";
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from "react-redux";
import * as actions from '../redux/actions';
import {BASE_URL_API, GET_DOMAIN} from '../redux/actions/type';
function SignIn(props) {
    const navigation = useNavigation();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [errors, setErrors] = useState('');
    const {signIn} = React.useContext(AuthContext);
    const {getDomain} = React.useContext(AuthContext);
    const [textDomain, setTextDomain] = useState('');
    const {RemoveDomain} = React.useContext(AuthContext);

    const SignIn = async () => {
        let domain_get = await GET_DOMAIN();
        let url_api ="https://"+ domain_get +"/api/v1";
        let value_details = {
            'username': username,
            'password': password
        };
        let formBody = [];
        for (let property in value_details) {
            let encodeKey = encodeURIComponent(property);
            let encodeValue = encodeURIComponent(value_details[property]);
            formBody.push(encodeKey + "=" + encodeValue);
        }
        formBody = formBody.join("&");
        fetch(url_api, {
            method: 'POST',
            headers: {
                "Rest_key": "api_key",
                Accept : 'application/x-www-form-urlencoded',
                "Content-Type": "application/x-www-form-urlencoded",
            },
            body: formBody
        }).then((response) => response.json())
            .then((data) => {
                if(data.status == 'fail'){
                    alert(data.message);
                }else{
                    signIn(data.user_token);
                }
            })
            .catch((error) => {
                console.error(error);
            })
    };
    const domainClear = () => {
        AsyncStorage.clear();
        RemoveDomain();
    };

    useEffect( async () => { // thay thế componentDidmount và componentDidUpdate
    }, []);
    return(
        <View style={[GlobalCss.container, GlobalCss.bgColorSignIn, {justifyContent: 'center', alignItems: 'center'}]}>
            <FastImage source={{uri: 'https://landing.kizuna.vn/public/assets/img/20200226073755.1Logo%20Kizuna%20632x160.png'}} style={{height: 150, width: '60%'}} resizeMode='contain'/>
            <View style={[GlobalCss.bgColorWhite, {width: '80%', borderRadius: 10}]}>
                <Text style={{fontWeight: 'bold', fontSize: 20, marginLeft: 20, marginTop: 20}}>Đăng nhập</Text>
                <Form style={{padding: 20, paddingTop: 0}}>
                    <Item floatingLabel>
                        <Input onChangeText={(text) => setTextDomain(text)} placeholder='Tên miền'/>
                    </Item>
                    <Item floatingLabel>
                        <Input onChangeText={(text) => setUsername(text)} placeholder='Username'/>
                    </Item>
                    <Item floatingLabel>
                        <Input onChangeText={(text) => setPassword(text)} secureTextEntry={true}  placeholder='Password'/>
                    </Item>
                </Form>
                <TouchableOpacity style={{paddingLeft: 20}}>
                    <Text style={{color: '#3C40C6'}}>Quên mật khẩu?</Text>
                </TouchableOpacity>
                <Button block style={{backgroundColor: '#FC427B', margin: 20, borderRadius: 10}} onPress={() => signIn()}>
                {/*<Button block style={{backgroundColor: '#FC427B', margin: 20, borderRadius: 10}} onPress={() => getDomain(textDomain)}>*/}
                    <Text style={{color: '#fff', fontWeight: 'bold'}}>Đăng nhập</Text>
                </Button>
            </View>
        </View>
    )
}

const mapStateToProps = state => ({
    loginGetTokenInfo: state.loginGetTokenInfo

});
export default connect(mapStateToProps, actions)(SignIn);
