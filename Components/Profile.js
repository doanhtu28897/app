import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, SafeAreaView, Text, StyleSheet} from 'react-native';
import FastImage from "react-native-fast-image";
import {Icon, Row} from 'native-base'
import GlobalCss from "../assets/style/GlobalCss";
import {useNavigation} from "@react-navigation/native";
import {AuthContext} from "./Auth/authContext";
import {SvgCssUri} from 'react-native-svg';
export default function Profile(props) {
    const navigation = useNavigation();

    const {Logout} = React.useContext(AuthContext);

    useEffect(() => {

    }, []);

    return(
        <View style={[GlobalCss.bgColorProfile, GlobalCss.container]}>
            <SafeAreaView style={{justifyContent: 'center', alignItems: 'center'}}>
                <Text style={styles.textTitle}>Profile</Text>
            </SafeAreaView>
            <Row size={1}/>
            <View>
                <View style={[GlobalCss.bgColorWhite, GlobalCss.viewAvatarProfile]}>
                    <FastImage source={{uri: 'https://www.w3schools.com/howto/img_avatar.png'}} style={GlobalCss.avatarProfile}/>
                </View>
                <Text style={GlobalCss.nameProfile}>Nhân viên ABC</Text>
                <View style={[GlobalCss.bgColorWhite, GlobalCss.viewMemberProfile]}>
                    <Text style={GlobalCss.textMemberProfile}>Thành viên</Text>
                </View>
            </View>
            <Row size={5}/>
            <View style={[GlobalCss.bgColorWhite, GlobalCss.viewBgProfile]}>
                <TouchableOpacity style={GlobalCss.touchViewHome}>
                    <View style={GlobalCss.viewIconProfile}>
                        <SvgCssUri
                            width={24}
                            height={22}
                            fill={'#000'}
                            stroke={'#000'}
                            uri= {"https://build.viin.vn/img_kizuna/notification.svg"}
                            style={GlobalCss.iconHome}
                        />
                    </View>
                    <View style={GlobalCss.viewNotiProfile}>
                        <Text style={styles.sizeTextNoti}>Thông báo</Text>
                        <Icon name='chevron-right' type='FontAwesome' style={GlobalCss.iconRight}/>
                    </View>

                </TouchableOpacity>
                <TouchableOpacity style={GlobalCss.touchViewHome}>
                    <View style={GlobalCss.viewIconProfile}>
                        <SvgCssUri
                            width={24}
                            height={22}
                            fill={'#000'}
                            stroke={'#000'}
                            uri= {"https://build.viin.vn/img_kizuna/user.svg"}
                            style={GlobalCss.iconHome}
                        />
                    </View>
                    <View style={GlobalCss.viewNotiProfile}>
                        <Text style={styles.sizeTextNoti}>Thông tin</Text>
                        <Icon name='chevron-right' type='FontAwesome' style={GlobalCss.iconRight}/>
                    </View>

                </TouchableOpacity>
                <TouchableOpacity style={GlobalCss.touchViewHome} onPress={() => Logout(null)}>
                    <View style={GlobalCss.viewIconProfile}>
                        <FastImage source={{uri: 'https://cdn-icons-png.flaticon.com/512/1329/1329958.png'}} style={GlobalCss.iconHome}/>
                    </View>
                    <View style={GlobalCss.viewNotiProfile}>
                        <Text style={styles.sizeTextNoti}>Log Out</Text>
                        <Icon name='chevron-right' type='FontAwesome' style={GlobalCss.iconRight}/>
                    </View>

                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    textTitle: {
        fontSize: 20,
        color: '#444444',
        fontWeight: 'bold'
    },
    sizeTextNoti: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#2C1F44'
    }
})
