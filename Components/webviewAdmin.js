import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {connect} from "react-redux";
import * as actions from "../redux/actions";
import { WebView } from 'react-native-webview';
import {GET_DOMAIN} from '../redux/actions/type';

function webviewAdmin(props){
    const [domainGet, setDomainGet] = useState('');
    const getDomain = async() => {
        let domain =  await GET_DOMAIN();
        setDomainGet(domain);
    };
    React.useEffect( () => { // thay thế componentDidmount và componentDidUpdate
        props.fetchLoginGetToken();
        getDomain();
    }, []);
    return(
        props.fetchLoginGetToken.actionload.length != 0 && domainGet != ''?
            <WebView
                source={{
                    uri: 'https://'+ domainGet +'/api/v1',
                    headers: {'User-token': props.fetchLoginGetToken.actionload}
                }}
            />: <Text/>

    )
}


const mapStateToProps = state => ({
    loginGetTokenInfo: state.loginGetTokenInfo
});
export default connect(mapStateToProps, actions)(webviewAdmin);
