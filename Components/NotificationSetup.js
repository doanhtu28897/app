import React, {Component} from 'react';
import * as actions from './redux/actions';
import firebase from "react-native-firebase";
import AsyncStorage from '@react-native-community/async-storage';
import Notification from 'react-native-firebase';
import {useNavigation} from '@react-navigation/native';
import * as RootNavigation from '../RootNavigation';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";

function NotificationSetup(props) {
    const [isLoading, setIsLoading] = React.useState(true);
    React.useEffect( () => { // thay thế componentDidmount và componentDidUpdate
        checkPermission();
        createNotificationListeners();
        /* const notification = new firebase.notifications.Notification()
              .setTitle('My notification title')
              .setBody('My notification body')
              .setData({
                  key1: 'value1',
                  key2: 'value2',
              });
          firebase.notifications().displayNotification(notification);*/
    }, []);

    const checkPermission = async () => {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            getToken();
        }
        else {
            requestPermission();
        }
    };
    const requestPermission = async () => {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            getToken();
        } catch (error) {
            // User has rejected permissions
            console.log('quyền bị từ chối');
        }
    };
    const getToken = async () => {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            //    console.log('token = ', fcmToken);
            if (fcmToken) {
                // user has a device token
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }
    };

    const createNotificationListeners = async () => {
        const channel_id = "b88_channel";
        const channel = new firebase.notifications.Android.Channel(channel_id, 'Test Channel', firebase.notifications.Android.Importance.Max)
            .setDescription('Channel mới được tạo');
        await firebase.notifications().android.createChannel(channel);
        await firebase.messaging().subscribeToTopic('b88');
        //Tạo channel
        await firebase.notifications().setBadge(1); //to set badge number
        //Vietnamese explain: khi đang ở foreground => show alert khi có noti..
        const onNotification =  async (notification: Notification) => {
            notification.android.setChannelId(channel_id);
            const notification_format = new firebase.notifications.Notification()
                .setTitle(notification._title)
                .setBody(notification._body)
                .setSound('default');
            showMessage({
                message: notification._title,
                description: notification._body,
                type: "info",
                duration: 8000
            });
            props.fetchListPaymentNew();
            await firebase.notifications().displayNotification(notification_format);
        };
        const notificationListener = await firebase.notifications().onNotification((noti) => {
            onNotification(noti);
        });
        const notificationOpenedListener = await firebase.notifications().onNotificationOpened((notificationOpen) => {
            //  alert('mở khi app còn ẩn dưới');
            // console.log(notificationOpen);
            const {title, body} = notificationOpen.notification;
            RootNavigation.navigate('HomeScreen');
        });
        /*
         * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
         * */
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            // console.log(notificationOpen);
            // alert('Mở khi app tắt hoàn toàn');
            RootNavigation.navigate('HomeScreen');
        }
        const messageListener = await firebase.messaging().onMessage((message) => {
            //process data message
            alert('message nè');
            //   console.log(JSON.stringify(message));
        });
    };
    return (
        <View>
            <FlashMessage position="top" />
        </View>

    );
}
const mapStateToProps = state => ({
    mobileNotificationInfo: state.mobileNotificationInfo
});
export default connect(mapStateToProps, actions)(NotificationSetup);
