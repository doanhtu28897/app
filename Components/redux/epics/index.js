import {combineEpics} from "redux-observable";
import InfoEpic from "./InfoEpic";
import loginGetTokenEpic from "./loginGetTokenEpic";
import MobileNotificationEpic from "./MobileNotificationEpic";
import readNotificationAllEpic from "./readNotificationAllEpic";
import readNotificationEpic from "./readNotificationEpic";
export default combineEpics(
    InfoEpic,
    loginGetTokenEpic,
    MobileNotificationEpic,
    readNotificationAllEpic,
    readNotificationEpic
)
