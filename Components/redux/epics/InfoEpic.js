import React, {Component} from 'react';
import * as actionTypes from '../actions/type';
import {from, of, throwError} from 'rxjs';
import {mergeMap, map, takeUntil, delay, tap, catchError, switchMap, concatMap} from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { ajax } from 'rxjs/ajax';
import {fromPromise} from 'rxjs/internal-compatibility';
let api_url = "";
let form_body = '';
let token = '';
const ajaxControl = (user_token, action) =>
    ajax({
        url: api_url,
        timeout: 5000,
        method: 'POST',
        headers: {
            "Rest-key" : "api_key",
            'User-token': user_token,
            "Content-Type": "application/x-www-form-urlencoded",
        },
        body: {

        }
    }).pipe(
        map(response => ({
            type: actionTypes.FETCH_INFO_SUCCESS,
            payload: response.response
        })),
        catchError(error => {
            //   console.log(error.response);
            //   return of(error.response);
            let data_error = {
                type: actionTypes.FETCH_INFO_ERROR,
                payload: {connect: false}
            };
            return of(data_error);
        })
    );

const InfoEpic = (action$, store) => action$.pipe(
    ofType(actionTypes.FETCH_INFO),
    concatMap(action => fromPromise(actionTypes.GET_TOKEN()).pipe(
        map(res =>  {
            token = res;
        })
    ).pipe(concatMap( () => fromPromise(actionTypes.GET_DOMAIN()).pipe(
        map( sub => {
            api_url = actionTypes.GET_URL(sub) + "MobileInformation";
        })))
    ).pipe(switchMap(() => ajaxControl(token))))
);


export default InfoEpic;
