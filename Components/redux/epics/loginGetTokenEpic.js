import React, {Component} from 'react';
import * as actionTypes from '../actions/type';
import { of } from 'rxjs'
import {mergeMap, map, takeUntil, delay, tap, catchError, switchMap, concatMap, flatMap} from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { ajax } from 'rxjs/ajax';
import {fromPromise} from 'rxjs/internal-compatibility';
let token = '';
const loginGetTokenEpic = (action$, store) => action$.pipe(
    ofType(actionTypes.FETCH_LOGIN_GET_TOKEN),
    concatMap(action => fromPromise(actionTypes.GET_TOKEN()).pipe(
        map(res =>  {
            token = res;
        })
    ).pipe(switchMap(() =>  {
        let data = {
            type: actionTypes.FETCH_LOGIN_GET_TOKEN_SUCCESS,
            payload: token
        };
        return of(data);
    })))
);
/*const memberEpic = (action$, store) => action$.pipe(
    ofType(actionTypes.FETCH_MEMBER),
    mergeMap(action => ajaxControl())
).pipe(delay(1000));*/

export default loginGetTokenEpic;
