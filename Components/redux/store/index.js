import {createStore, applyMiddleware, compose} from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import reducers from '../reducers';
import rootEpic from '../epics';
const epicMiddleware = createEpicMiddleware();
const store = createStore(
    reducers,
    applyMiddleware(epicMiddleware),
);
epicMiddleware.run(rootEpic);
export default store;
