import AsyncStorage from '@react-native-community/async-storage';
export const GET_DOMAIN = async () => {
    return await AsyncStorage.getItem('domain');
};
export const BASE_URL = 'http://tac.adnz.xyz:8118';
export const BASE_URL_API = 'http://tac.adnz.xyz:8118/api/v1';
export const REST_KEY = 'api_key';

export const GET_TOKEN = () => {
    return AsyncStorage.getItem('userToken');
};
export const GET_URL = (domain) => {
    return "https://" + domain + "/api/v1/"
};

export const FETCH_LOGIN_GET_TOKEN = 'FETCH_LOGIN_GET_TOKEN';
export const FETCH_LOGIN_GET_TOKEN_SUCCESS = 'FETCH_LOGIN_GET_TOKEN_SUCCESS';
export const FETCH_LOGIN_GET_TOKEN_ERROR = 'FETCH_LOGIN_GET_TOKEN_ERROR';

export const FETCH_INFO = 'FETCH_INFO';
export const FETCH_INFO_SUCCESS = 'FETCH_INFO_SUCCESS';
export const FETCH_INFO_ERROR = 'FETCH_INFO_ERROR';

export const FETCH_MOBILE_NOTIFICATION = 'FETCH_MOBILE_NOTIFICATION';
export const FETCH_MOBILE_NOTIFICATION_SUCCESS = 'FETCH_MOBILE_NOTIFICATION_SUCCESS';
export const FETCH_MOBILE_NOTIFICATION_ERROR = 'FETCH_MOBILE_NOTIFICATION_ERROR';

export const FETCH_READ_NOTIFICATION = 'FETCH_READ_NOTIFICATION';
export const FETCH_READ_NOTIFICATION_SUCCESS = 'FETCH_READ_NOTIFICATION_SUCCESS';
export const FETCH_READ_NOTIFICATION_ERROR = 'FETCH_READ_NOTIFICATION_ERROR';

export const FETCH_READ_NOTIFICATION_ALL = 'FETCH_READ_NOTIFICATION_ALL';
export const FETCH_READ_NOTIFICATION_ALL_SUCCESS = 'FETCH_READ_NOTIFICATION_ALL_SUCCESS';
export const FETCH_READ_NOTIFICATION_ALL_ERROR = 'FETCH_READ_NOTIFICATION_ALL_ERROR';
