import * as actionTypes from '../actions/type'

export const fetchLoginGetToke = () => {
    return {type: actionTypes.FETCH_LOGIN_GET_TOKEN};
};

export const fetchInfo = (username, password) => {
    return {type: actionTypes.FETCH_INFO, username: username, password: password};
};

export const fetchMobileNotification = () => {
    return {type: actionTypes.FETCH_MOBILE_NOTIFICATION};
};

export const fetchReadNotification = (id) => {
    return {type: actionTypes.FETCH_READ_NOTIFICATION, id: id};
};

export const fetchReadNotificationAll = () => {
    return {type: actionTypes.FETCH_READ_NOTIFICATION_ALL};
};
