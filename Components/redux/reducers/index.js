import {combineReducers} from 'redux';
import InfoReducer from "./InfoReducer";
import loginGetTokenReducer from "./loginGetTokenReducer";
import MobileNotificationReducer from "./MobileNotificationReducer";
import readNotificationReducer from "./readNotificationReducer";
import readNotificationAllReducer from "./readNotificationAllReducer";
export default combineReducers(    {
    loginGetTokenInfo: loginGetTokenReducer,
    infoReducerInfo: InfoReducer,
    mobileNotificationInfo: MobileNotificationReducer,
    readNotificationInfo: readNotificationReducer,
    readNotificationAllInfo: readNotificationAllReducer
});
