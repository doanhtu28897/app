import * as actionTypes from '../actions/type';
const initialState = {
    actionload: [],
    isLoading: false
};
export default function (state = initialState, action) {
    switch (action.type) {
        case actionTypes.FETCH_MOBILE_NOTIFICATION:
            return {
                ...state,
                isLoading: true
            };
        case actionTypes.FETCH_MOBILE_NOTIFICATION_SUCCESS:
            return {
                ...action.payload,
                isLoading: false,
            };
        case actionTypes.FETCH_MOBILE_NOTIFICATION_ERROR:
            return {
                ...action.payload,
                isLoading: false,
            };
        default:
            return state;
    }
}
