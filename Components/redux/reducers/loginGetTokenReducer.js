import * as actionTypes from '../actions/type';
const initialState = {
    actionload: [],
    isLoading: false
};
export default function (state = initialState, action) {
    switch (action.type) {
        case actionTypes.FETCH_LOGIN_GET_TOKEN:
            return {
                ...state,
                isLoading: true
            };
        case actionTypes.FETCH_LOGIN_GET_TOKEN_SUCCESS:
            return {
                actionload: action.payload,
                isLoading: false,
            };
        case actionTypes.FETCH_LOGIN_GET_TOKEN_ERROR:
            return {
                actionload: action.payload,
                isLoading: false,
            };
        default:
            return state;
    }
}
