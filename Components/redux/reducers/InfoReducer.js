import * as actionTypes from '../actions/type';
const initialState = {
    //  actionload: [],
    isLoading: false
};
export default function (state = initialState, action) {
    switch (action.type) {
        case actionTypes.FETCH_INFO:
            return {
                ...state,
                isLoading: true
            };
        case actionTypes.FETCH_INFO_SUCCESS:
            return {
                ...action.payload,
                isLoading: false,
            };
        case actionTypes.FETCH_INFO_ERROR:
            return {
                ...action.payload,
                isLoading: false,
            };
        default:
            return state;
    }
}
