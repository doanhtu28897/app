import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, SafeAreaView, Text, StyleSheet} from 'react-native';
import FastImage from "react-native-fast-image";
import {Icon} from 'native-base'
import GlobalCss from "../assets/style/GlobalCss";
import {useNavigation} from '@react-navigation/native'

export default function Tracking(props){
    const navigation = useNavigation();

    useEffect(() => {

    }, []);

    return(
        <View style={[GlobalCss.container, GlobalCss.bgColorWhite]}>
            <SafeAreaView style={GlobalCss.bgColorLoadingPage}>
                <View style={GlobalCss.view_title_header}>
                    <View style={[GlobalCss.iconBack, {width: '20%'}]}/>
                    <View style={GlobalCss.titleWidth}>
                        <Text  style={GlobalCss.title_header_text}>Tracking</Text>
                    </View>
                    <View style={[GlobalCss.iconBack, {width: '20%'}]}/>
                </View>
            </SafeAreaView>

        </View>
    )
}

const styles = StyleSheet.create({
    sizeText: {
        fontSize: 18
    }
})
