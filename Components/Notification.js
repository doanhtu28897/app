import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, SafeAreaView, Text, StyleSheet} from 'react-native';
import FastImage from "react-native-fast-image";
import {Icon} from 'native-base'
import GlobalCss from "../assets/style/GlobalCss";
import {useNavigation} from '@react-navigation/native'
import {SvgCssUri} from 'react-native-svg';
export default function Notification(props) {

    const navigation = useNavigation();

    useEffect(() => {

    }, []);
    return(
        <View style={[GlobalCss.container, GlobalCss.bgColorWhite]}>
            <SafeAreaView style={GlobalCss.bgColorLoadingPage}>
                <View style={GlobalCss.view_title_header}>
                    <View style={[GlobalCss.iconBack, {width: '20%'}]}/>
                    <View style={GlobalCss.titleWidth}>
                        <Text  style={GlobalCss.title_header_text}>Thông báo</Text>
                    </View>
                    <View style={[GlobalCss.iconBack, {width: '20%'}]}/>
                </View>
            </SafeAreaView>
            <View style={{padding: 15}}>
                <TouchableOpacity style={GlobalCss.touchViewHome}>
                    <View style={GlobalCss.viewIconHome}>
                        <SvgCssUri
                            width={24}
                            height={22}
                            uri= {"https://build.viin.vn/img_kizuna/flame.svg"}
                            style={GlobalCss.iconHome}
                        />
                    </View>
                    <View style={GlobalCss.flexRow}>
                        <View>
                            <Text style={styles.textTitle}>Thông báo cháy</Text>
                            <Text style={styles.textContent}>Có 1 sự cố cháy tại abc...</Text>
                        </View>
                        <Icon name='chevron-right' type='FontAwesome' style={GlobalCss.iconRight}/>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={GlobalCss.touchViewHome}>
                    <View style={GlobalCss.viewIconHome}>
                        <SvgCssUri
                            width={24}
                            height={22}
                            uri= {"https://build.viin.vn/img_kizuna/shield.svg"}
                            style={GlobalCss.iconHome}
                        />
                    </View>
                    <View style={GlobalCss.flexRow}>
                        <View>
                            <Text style={styles.textTitle}>Thông báo an ninh</Text>
                            <Text style={styles.textContent}>Khu vực ABC đã có camera giám sát</Text>
                        </View>
                        <Icon name='chevron-right' type='FontAwesome' style={GlobalCss.iconRight}/>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={GlobalCss.touchViewHome}>
                    <View style={GlobalCss.viewIconHome}>
                        <SvgCssUri
                            width={24}
                            height={22}
                            fill={'#fff'}
                            stroke={'#fff'}
                            uri= {"https://build.viin.vn/img_kizuna/plug.svg"}
                            style={GlobalCss.iconHome}
                        />
                    </View>
                    <View style={GlobalCss.flexRow}>
                        <View>
                            <Text style={styles.textTitle}>Thông báo nguồn điện</Text>
                            <Text style={styles.textContent}>Nguồn điện đã được phục hồi...</Text>
                        </View>
                        <Icon name='chevron-right' type='FontAwesome' style={GlobalCss.iconRight}/>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={GlobalCss.touchViewHome}>
                    <View style={GlobalCss.viewIconHome}>
                        <SvgCssUri
                            width={24}
                            height={22}
                            uri= {"https://build.viin.vn/img_kizuna/elevator.svg"}
                            style={GlobalCss.iconHome}
                        />
                    </View>
                    <View style={GlobalCss.flexRow}>
                        <View>
                            <Text style={styles.textTitle}>Thông báo lỗi thang máy</Text>
                            <Text style={styles.textContent}>Thang máy ABC đang được sửa chữa...</Text>
                        </View>
                        <Icon name='chevron-right' type='FontAwesome' style={GlobalCss.iconRight}/>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={GlobalCss.touchViewHome}>
                    <View style={GlobalCss.viewIconHome}>
                        <SvgCssUri
                            width={24}
                            height={22}
                            uri= {"https://build.viin.vn/img_kizuna/checklist.svg"}
                            style={GlobalCss.iconHome}
                        />
                    </View>
                    <View style={GlobalCss.flexRow}>
                        <View>
                            <Text style={styles.textTitle}>Đã chốt tiền điện/chỉ số</Text>
                            <Text style={styles.textContent}>Tiền điện tháng 12 đã được chốt...</Text>
                        </View>
                        <Icon name='chevron-right' type='FontAwesome' style={GlobalCss.iconRight}/>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    textTitle: {
        fontSize: 18,
        color: '#444444'
    },
    textContent: {
        fontSize: 12,
        color: '#FC427B'
    }
})
